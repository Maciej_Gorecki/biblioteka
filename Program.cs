﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    
    class Program
    {
        

        static void Main(string[] args)
        {
            Ksiazka k = new Ksiazka();
            string x;
            do { 
                Console.WriteLine(" Co chcesz zrobić? " + Environment.NewLine + " 1 - sprawdzic ksiazke \n 2 - dodac ksiazke \n 3 - edytowac ksiazke\n 4 - usunac ksiazke\n 5 - eksport/import\n 0 - zamknac program\n");
                x = Console.ReadLine();
                switch (x)
                {
                    case "1":
                        k.sprawdzksiazke();
                        break;
                    case "2":
                        k.dodajksiazke();
                        break;
                    case "3":
                        k.edytujksiazke();
                        break;
                    case "4":
                        k.usunksiazke();
                        break;
                    case "5":
                        k.wyslij();
                        break;
                    default:
                        Console.WriteLine("Podaj właściwą opcję!");
                        Console.ReadLine();
                        break;
                }
            }
            while (x != "0");



        }
    }
}
