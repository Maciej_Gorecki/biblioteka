﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    public class BialyKruk : Ksiazka
    {
        public string Historia;
        public BialyKruk()
        { }
        public BialyKruk(string tytul, string gatunek, string wydawnictwo, string opis, string historia) : base(tytul, gatunek, wydawnictwo, opis)
        {
            Historia = historia;
        }
        public override void wyswietl()
        {
            base.wyswietl();
            Console.WriteLine("Historia: {0}", Historia);
        }
    }
}
