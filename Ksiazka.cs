﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace ConsoleApplication3
{
    //dziedziczenie metody virtual dla strony bazowej, override dla dziedziczącej 
    public class Ksiazka
    {
        


        static public IList<Ksiazka> ksiazki = new List<Ksiazka>() { new Ksiazka("cokolwiek1", "cokolwiek2", "cokolwiek3", "cokolwiek4") };

        
        public string Tytul;
        public string Gatunek;
        public string Wydawnictwo;
        public string Opis;
        public Ksiazka() { }
        public Ksiazka(string tytul, string gatunek, string wydawnictwo,string opis) {
            Tytul = tytul;
            Gatunek = gatunek;
            Wydawnictwo = wydawnictwo;
            Opis = opis;
        }
        public void dodawanie()
        {
            Console.WriteLine("Tytul:\n");
            Tytul = Console.ReadLine();
            Console.WriteLine("Gatunek:\n");
            Gatunek = Console.ReadLine();
            Console.WriteLine("Wydawnictwo:\n");
            Wydawnictwo = Console.ReadLine();
            Console.WriteLine("Opis:\n");
            Opis = Console.ReadLine();
        }
        public virtual void wyswietl()
        {
            Console.WriteLine("Tytuł: {0}\nGatunek: {1}\nWydawnictwo: {2}\nOpis:  {3}", Tytul, Gatunek, Wydawnictwo, Opis);
        }
        public void dodajksiazke()
        {
            string dodaj;
            do
            {

                Console.WriteLine("Co chcesz dodać?");
                Console.WriteLine("1. Zwykłą książkę");
                Console.WriteLine("2. Lekturę");
                Console.WriteLine("3. Białego kruka");
                Console.WriteLine("0. Powrót\n");
                dodaj = Console.ReadLine();
                switch (dodaj)
                {
                    case "1":
                        dodawanie();
                        ksiazki.Add(new Ksiazka(Tytul, Gatunek, Wydawnictwo, Opis));
                        break;
                    case "2":
                        dodawanie();
                        Console.WriteLine("Klasa:");
                        string klasa = Console.ReadLine();
                        ksiazki.Add(new Lektury(Tytul, Gatunek, Wydawnictwo, Opis, klasa));
                        break;
                    case "3":
                        dodawanie();
                        Console.WriteLine("Historia:");
                        string kruk = Console.ReadLine();
                        ksiazki.Add(new BialyKruk(Tytul, Gatunek, Wydawnictwo, Opis, kruk));
                        break;
                    case "0":
                        break;
                    default:
                        Console.WriteLine("Wybierz właściwą opcję!");
                        break;

                }
            }
            while (dodaj != "0"); 
        }
        public void sprawdzksiazke()
        {
            string sprawdz;
            do
            {
                Console.WriteLine("Wybierz opcje wyszukiwania: ");
                Console.WriteLine("1. Tytul");
                Console.WriteLine("2. Gatunek ");
                Console.WriteLine("3. Wydawnictwo ");
                Console.WriteLine("4. Opis");
                Console.WriteLine("5. Historia(dotyczy tylko Białych Kruków)");
                Console.WriteLine("6. Klasa(dotyczy tylko lektur)");
                Console.WriteLine("7. Wyświetl wszystko");
                Console.WriteLine("0. Powrót");
                sprawdz = Console.ReadLine();
                switch (sprawdz)
                {
                    case "1":
                        Console.WriteLine("Wpisz tytuł: ");
                        string tytul2 = Console.ReadLine();
                        var wybrana = ksiazki.Where(x => x.Tytul.Contains(tytul2));
                        foreach (var kejs1 in wybrana)
                        {
                            kejs1.wyswietl();
                        }
                        break;
                    case "2":
                        Console.WriteLine("Wpisz gatunek: ");
                        string gat = Console.ReadLine();
                        var wybrana2 = ksiazki.Where(x => x.Gatunek.Contains(gat)).ToList();
                        foreach (var kejs2 in wybrana2)
                        {
                            kejs2.wyswietl();
                        }
                        break;
                    case "3":
                        Console.WriteLine("Wydawnictwo: ");
                        string wyd = Console.ReadLine();
                        var wybrana3 = ksiazki.Where(x => x.Wydawnictwo.Contains(wyd));
                        foreach (var kejs3 in wybrana3)
                        {
                            kejs3.wyswietl();
                        }
                        break;
                    case "4":
                        Console.WriteLine("Wpisz opis: ");
                        string opis2 = Console.ReadLine();
                        var wybrana4 = ksiazki.Where(x => x.Opis.Contains(opis2));
                        foreach (var kejs4 in wybrana4)
                        {
                            kejs4.wyswietl();
                        }
                        break;
                     case "5":
                         BialyKruk b = new BialyKruk();
                         
                         Console.WriteLine("Wpisz historię: ");
                         string hist2 = Console.ReadLine();
                        var wybrana5 = ksiazki.Where(x => x is BialyKruk && (x as BialyKruk).Historia.Contains(hist2));
                         foreach(var kejs5 in wybrana5)
                         {
                             kejs5.wyswietl();
                         }
                         break;
                     case "6":
                         Lektury l = new Lektury();
                         Console.WriteLine("Klasa: ");
                         string klasa2 = Console.ReadLine();
                         var wybrana6 = ksiazki.Where(x => x is Lektury && (x as Lektury).Klasa.Contains(klasa2));
                         foreach (var kejs6 in wybrana6)
                         {
                             kejs6.wyswietl();
                         }
                         break;
                    case "7":
                        foreach(var wszystko in ksiazki)
                        {
                            wszystko.wyswietl();
                           
                        }
                        Console.ReadLine();
                        break;
                    case "0":
                        break;
                    default:
                        Console.WriteLine("Podaj właściwą opcję!");
                        break;

                }
            }
            while (sprawdz != "0");



        }
        public void edytujksiazke()
        {
            string edytuj;

                int liczedyt = 0;
                foreach (var ia in ksiazki)
                {
                    liczedyt += 1;
                    Console.WriteLine("{1}. {0}", ia.Tytul, liczedyt);
                }
                Console.WriteLine("0. Powrót\nPodaj nr pozycji, która ma być edytowana: ");
                var doedycji = Console.ReadLine();
                int edytint;
                int.TryParse(doedycji, out edytint);
                if ((edytint > 0) && (edytint < 999999))
                {
                   
                        Console.WriteLine("Co chcesz edytować?");
                        Console.WriteLine("1. Tytuł");
                        Console.WriteLine("2. Gatunek");
                        Console.WriteLine("3. Wydawnictwo");
                        Console.WriteLine("4. Opis");
                        Console.WriteLine("5. Historia(dotyczy Białych Kruków)");
                        Console.WriteLine("6. Klasa(dotyczy lektur)");
                        Console.WriteLine("0. Powrót");
                        edytuj = Console.ReadLine();
                        switch (edytuj)
                        {
                            case "1":
                                Console.WriteLine("Obecny tytuł: " + ksiazki[edytint - 1].Tytul);
                                Console.WriteLine("Nowy tytuł: ");
                                string nowytyt = Console.ReadLine();
                                ksiazki[edytint - 1].Tytul = nowytyt;
                                break;
                            case "2":
                                Console.WriteLine("Obecny gatunek: " + ksiazki[edytint - 1].Gatunek);
                                Console.WriteLine("Nowy gatunek: ");
                                string nowygat = Console.ReadLine();
                                ksiazki[edytint - 1].Gatunek = nowygat;
                                break;
                            case "3":
                                Console.WriteLine("Obecne wydawnictwo: " + ksiazki[edytint - 1].Wydawnictwo);
                                Console.WriteLine("Nowe wydawnictwo: ");
                                string nowywyd = Console.ReadLine();
                                ksiazki[edytint - 1].Wydawnictwo = nowywyd;
                                break;
                            case "4":
                                Console.WriteLine("Obecny opis: " + ksiazki[edytint - 1].Opis);
                                Console.WriteLine("Nowy opis: ");
                                string nowyopis = Console.ReadLine();
                                ksiazki[edytint - 1].Opis = nowyopis;
                                break;
                            case "5":
                                if (ksiazki[edytint - 1] is BialyKruk)
                                {
                                    Console.WriteLine("Obecna historia: {0}", (ksiazki[edytint - 1] as BialyKruk).Historia);
                                    Console.WriteLine("Nowa historia: ");
                                    string nowahist = Console.ReadLine();
                                    (ksiazki[edytint - 1] as BialyKruk).Historia = nowahist;
                                }
                                
                                else
                                {
                                    Console.WriteLine("Książka nie jest białym krukiem!");
                                    Console.ReadLine();
                                }

                                break;
                            case "6":
                                if (ksiazki[edytint - 1] is Lektury)
                                {
                                    Console.WriteLine("Obecna klasa: {0}", (ksiazki[edytint - 1] as Lektury).Klasa);
                                    Console.WriteLine("Nowa klasa: ");
                                    string nowaklasa = Console.ReadLine();
                                    (ksiazki[edytint - 1] as Lektury).Klasa = nowaklasa;
                                }
                                else
                                {
                                    Console.WriteLine("Książka nie jest lekturą!");
                                    Console.ReadLine();
                                }
                                break;
                            
                            default:
                                Console.WriteLine("Wybierz właściwą opcję!");
                                Console.ReadLine();
                                break;
                        }

                    }
                else if (edytint == 0)
                {

                }

          
                else
                {
                    Console.WriteLine("Podaj poprawną opcję!");
                    Console.ReadLine();
                }
            
        }
        public void usunksiazke() {

            for (int i = 0; i < ksiazki.Count; i++)
            {
                Console.WriteLine("Nr {0}, Tytul {1}", i+1, ksiazki[i].Tytul);
            }
            Console.WriteLine("Podaj nr książki, którą chcesz usunąć\n0 żeby powrócić");

            var usun = Console.ReadLine();
            int usunint;
            int.TryParse(usun, out usunint);
            if ((usunint < 999999) && (usunint > 0))
            {

                ksiazki.RemoveAt(usunint-1);
                Console.WriteLine("Książka została usunięta");
            }
            else if (usunint == 0)
            {

            }
            else
            { 
                Console.Read();
            }
           
        }
        public void wyslij()
        {
            string plik;
            Console.WriteLine("1. Eksportuj do pliku na pulpicie");
            Console.WriteLine("2. Importuj z pliku");
            Console.WriteLine("0. Powrót");
            string wysylanie = Console.ReadLine();
            switch (wysylanie)
            {
                case "1":
                    string Dana_ksiazka = JsonConvert.SerializeObject(ksiazki, Formatting.Indented, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All });
                    string pulpit = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                    plik = Path.Combine(pulpit, "plik.txt");
                    File.WriteAllText(@"C:\Users\User\Desktop\plik.txt", Dana_ksiazka);
                    Console.WriteLine("Ksiazki zostaly zapisane do plik.txt na pulpicie użytkownika User");
                    Console.WriteLine();
                    break;
                case "2":
                    string pulpit2 = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                    plik = Path.Combine(pulpit2, "plik.txt");
                    ksiazki = JsonConvert.DeserializeObject<List<Ksiazka>>(File.ReadAllText(@"C:\Users\User\Desktop\plik.txt"), new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All });
                    Console.WriteLine("Wczytano listę pozycji");
                    Console.WriteLine();
                    break;


            }

        }
    }
}
