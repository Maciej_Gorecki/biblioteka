﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Lektury : Ksiazka
    {
        public string Klasa;
        public Lektury() { }
        public Lektury(string tytul, string gatunek, string wydawnictwo, string opis, string klasa) : base(tytul,gatunek,wydawnictwo,opis)
        {
            Klasa = klasa;
        }
        public override void wyswietl()
        {
            base.wyswietl();
            Console.WriteLine("Klasa: {0}", Klasa);
        }
        
           
        
    }
}
